<#
.SYNOPSIS
   Check les taches planifi�es qui tournent depuis plus de 6h00
.DESCRIPTION
   Thierry Logeais - 08/2015
.PARAMETER <paramName>
   Prend en param�tre un nom de taches (wildcards autoris�s)
.EXAMPLE
   
#>

Param ([parameter(Mandatory = $true)][string] $TaskName)
$Resultats = new-object System.Collections.ArrayList
$Result = ""

# Connects to the scheduled service on the local host and returns scheduled task infromation.

$Schedule = New-Object -Com("Schedule.Service")
$Schedule.Connect([System.Net.Dns]::GetHostName())
$RunningTasksInfos = Get-ScheduledTask -TaskName $TaskName | Where State -eq "Running" | Get-ScheduledTaskInfo
if (($RunningTasksInfos | Measure-Object).count -lt 1)
    {
		# Pas de taches qui tournent
		$returncode = 0
		"All is OK - '0 Task running'"
    }
    	else
    {
		# Au moins une tache tourne
		foreach ($task in $RunningTasksInfos) {
			$My_Task = "" | Select-Object Taskname,TimeStart,Now,Resultat,Duration
			$My_Task.TaskName = $task.Taskname
			$My_Task.TimeStart = $task.LastRunTime | Get-Date -UFormat %s
			$My_Task.Now = [int][double]::Parse((Get-Date -UFormat %s))
			
			# Si la dur�e de la tache est sup�rieure � 06h00
			if (($My_Task.Now - $My_Task.TimeStart) -gt 21600) {
				# on logue dans $resultats pour une sortie massive
				$My_Task.Duration = ($My_Task.Now - $My_Task.TimeStart) / 3600
				$My_Task.Resultat = " Task " + $My_Task.TaskName + " in progress for more than 6 hours.( " + [math]::round($My_Task.Duration,1) + " Heures)"
				$Resultats.Add($My_Task) | out-null
				$Result += $My_Task.Resultat
			}
		}
		
		if ((($Resultats | Measure-Object).count) -gt 0) {
				$returncode = 2
				#($Resultats | select Resultat)
				#$Result 
			}
			Else
			{	
				$returncode = 0
				$Result = "All is OK - 'No Task in progress for more than 6 hours'" 
			}
	} 
$Result
exit $returncode