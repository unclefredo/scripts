Param ([parameter(Mandatory = $true)][string] $TaskName)

# Connects to the scheduled service on the local host and returns scheduled task infromation.cdec
$Schedule = New-Object -Com("Schedule.Service")
$Schedule.Connect([System.Net.Dns]::GetHostName())
$Task = Get-ScheduledTask -TaskName $TaskName | Where State -eq "Ready" | Get-ScheduledTaskInfo | Where LastTaskResult -ne 0 | Select TaskName
$count =  (($Task | Measure-Object).count)
$result= "| 'Task in error'=" + $count."Task in error" + ";;;;"

If (!$Task) 
	{ 
	# Sets the  return code as 'OK'.
	$returncode = 0
    "All is OK - $count 'Task in error' $result" 
    }
    else
    {
	$returncode = 2
    "CRITICAL - $count 'Task in error' $result"
	} 
	
# Sets a service status message based on the task name, the last time the task run and the the result. 
#"The Scheduled Task " + $Task.Name + " last run on " + $Task.LastRunTime + " with the Last Run Result " + $Task.LastTaskResult

#Exits the session returning the exit code. 
#$returncode
exit $returncode