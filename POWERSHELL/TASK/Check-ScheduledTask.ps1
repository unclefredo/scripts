#======================================================================================================================================================================================
# NAME:   		Check-ScheduledTask.ps1 (For use with Nagios XI check_npre command)
# AUTHOR:		Dean Grant
# DATE:     	24/09/2013 
# VERSION:		1.0
#
# COMMENTS:  	Checks scheduled task and reports last run time and job status. 
#
# USAGE:        ./Check-ScheduledTask.ps1 -TaskName <Scheduled Task> - Hours <number of hours>
# EXAMPLE:      ./Check-ScheduledTask.ps1 -TaskName "Scheduled Task ABC" -Hours 1
#======================================================================================================================================================================================


# Specifies mandatory paramaters for task name and number of days since last run time.
Param ([parameter(Mandatory = $true)][string] $TaskName)
#[parameter(Mandatory = $true)][string] $Hours)
$Hours = "1"


# Connects to the scheduled service on the local host and returns scheduled task infromation.
$Schedule = New-Object -Com("Schedule.Service")
$Schedule.Connect([System.Net.Dns]::GetHostName())
$Task = $Schedule.GetFolder("\").GetTasks(0)| Where-Object {$_.Name -eq $TaskName} 

# Conditional logic to determine if the scheduled task last run result was successful and number of days since last run time and sets a return code.
If ($Task.LastRunTime -gt (get-date).AddHours(-$Hours) -and $Task.LastTaskResult -eq "0") 
	{ 
	# Sets the  return code as 'OK'.
	$returncode = 0 
	} 

If ($Task.LastRunTime -lt (get-date).AddHours(-$Hours) -or $Task.LastTaskResult -ne "0" )
	{ 
	# Sets the  return code as 'Critical'.
	$returncode = 2
	} 
	
# Sets a service status message based on the task name, the last time the task run and the the result. 
"The Scheduled Task " + $($Task.Name) + " last run on " + $($Task.LastRunTime) + " with the Last Run Result " + $($Task.LastTaskResult)

#Exits the session returning the exit code. 
exit $returncode
